ics-ans-swagger-ui
===================

Ansible playbook to install Swagger UI.

Requirements
------------

- ansible >= 2.4
- molecule >= 2.6

License
-------

BSD 2-clause
