import os
import testinfra.utils.ansible_runner

testinfra_hosts = testinfra.utils.ansible_runner.AnsibleRunner(
    os.environ['MOLECULE_INVENTORY_FILE']).get_hosts('swagger_ui')


def test_naming_swagger_api_url(host):
    cmd = host.run("curl --insecure --fail https://ics-ans-swagger-ui-default/naming/")
    assert cmd.rc == 0
    assert 'url: "https://gitlab.esss.lu.se/snippets/1/raw"' in cmd.stdout
